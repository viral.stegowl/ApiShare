<div class="page-wrapper-row">
    <div class="page-wrapper-top">
        <!-- BEGIN HEADER -->
        <div class="page-header">
            <!-- BEGIN HEADER TOP -->
            <div class="page-header-top">
                <div class="container">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <div class="logo" style="color: white; font-size: 25px; padding-top: 18px;">  
                            <i class="fa fa-file-text" aria-hidden="true" style="text-shadow: 0px 0px 5px black;"></i> <span style="color: red; font-weight: bold; text-shadow: 0px 0px 2px black;">Api</span> <span style="text-shadow: 0px 0px 5px black;">Share</span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler"></a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">    
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <li class="dropdown dropdown-user dropdown-dark">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="/assets/layouts/layout3/img/avatar9.jpg">
                                    <span class="username username-hide-mobile">{{Auth::user()->name}}</span>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    {{-- <li>
                                        <a href="page_user_profile_1.html">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>                                                
                                    <li>
                                        <a href="app_todo_2.html">
                                            <i class="icon-rocket"></i> My Tasks
                                            <span class="badge badge-success"> 7 </span>
                                        </a>
                                    </li> --}}
                                    <li>
                                        <a href="{{route('change.password')}}">
                                            <i class="fa fa-key" aria-hidden="true"></i> Change Password                                            
                                        </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="{{route('logout')}}">
                                            <i class="icon-key"></i> Log Out 
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->                            
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
            </div>
            <!-- END HEADER TOP -->
            <!-- BEGIN HEADER MENU -->
            <div class="page-header-menu">
                <div class="container">                                
                    <!-- BEGIN MEGA MENU -->
                    <!-- DOC: Apply "hor-menu-light" class after the "hor-menu" class below to have a horizontal menu with white background -->
                    <!-- DOC: Remove data-hover="dropdown" and data-close-others="true" attributes below to disable the dropdown opening on mouse hover -->
                    <div class="hor-menu  ">
                        <ul class="nav navbar-nav">

                        @if(Auth::user()->role === 1)

                            <li aria-haspopup="true" class=" ">
                                <a href="{{route('admin.dashboard')}}" class="nav-link  ">
                                    <i class="icon-bar-chart"></i> Dashboard
                                </a>
                            </li> 
                            <li aria-haspopup="true" class=" ">
                                <a href="{{route('admin.projects.index')}}" class="nav-link  ">
                                    <i class="fa fa-tasks" aria-hidden="true"></i> Projects
                                </a>
                            </li> 
                            <li aria-haspopup="true" class=" ">
                                <a href="{{route('admin.emp.index')}}" class="nav-link  ">
                                    <i class="fa fa-users" aria-hidden="true"></i> Employees
                                </a>
                            </li>
                            <li aria-haspopup="true" class=" ">
                                <a href="{{route('admin.assign.page')}}" class="nav-link  ">
                                    <i class="fa fa-tasks" aria-hidden="true"></i> Assign Project
                                </a>
                            </li>
                        @elseif(Auth::user()->role === 2)

                            <li aria-haspopup="true" class=" ">
                                <a href="{{route('creator.main')}}" class="nav-link">
                                    <i class="fa fa-tasks" aria-hidden="true"></i> Projects
                                </a>
                            </li>

                            <li aria-haspopup="true" class=" ">
                                <a href="{{route('creator.api.page')}}" class="nav-link">
                                    <i class="fa fa-random" aria-hidden="true"></i> Create Api
                                </a>
                            </li>

                        @else
                            <li aria-haspopup="true" class=" ">
                                <a href="{{route('creator.main')}}" class="nav-link">
                                    <i class="fa fa-tasks" aria-hidden="true"></i> Projects
                                </a>
                            </li>
                        @endif

                        </ul>
                    </div>
                    <!-- END MEGA MENU -->
                </div>
            </div>
            <!-- END HEADER MENU -->
        </div>
        <!-- END HEADER -->
    </div>
</div>
