@if(count($errors)>0)
	<script type="text/javascript">
	@foreach($errors->all() as $error)
		toastr.error('{{$error}}','System message');
	@endforeach
	</script>
@endif


@if(session('success'))
	<script type="text/javascript">
		toastr.success('{{session('success')}}','System message');
	</script>	
@endif

@if(session('error'))
	<script type="text/javascript">
		toastr.error('{{session('error')}}','System message');
	</script>
@endif
