@extends('layout.app')

@section('title','Your assigned Projects')

@section('content')
<div class="portlet" id="employee-list">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-users" aria-hidden="true"></i> Project Details and other options </div> 
            <a href="#" v-on:click.prevent="getNewProjects(currentPage)" class="btn red-mint btn-outline sbold uppercase pull-right" style="margin-left:5px;"><i class="fa fa-refresh" aria-hidden="true"></i></a>                   
        </div>
    <div class="portlet-title">
        <span class="pull-right">You are currently on page <strong>@{{currentPage}}</strong> of <strong>@{{lastPage}}</strong> pages.</span>

        <div class="col-md-1">            
            <select class="form-control" v-model="jumpPage" v-on:change.prevent="getNewProjects(jumpPage)">
                <option v-for="n in lastPage" v-bind:disabled="currentPage==n" v-bind:value="n">@{{n}}</option>
            </select>
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-scrollable">
            <table class="table table-striped table-bordered table-advance table-hover">
                <thead>
                    <tr>                        
                        <th class="hidden-xs"><i class="fa fa-user"></i> Project Name </th>
                        <th><i class="fa fa-shopping-cart"></i> Project Description </th>
                        <th><i class="fa fa-shopping-cart"></i> Employees </th>
                        <th><i class="fa fa-shopping-cart"></i> API Count</th>
                        <th><i class="fa fa-shopping-cart"></i> Project Status </th>
                        <th><i class="fa fa-shopping-cart"></i> Added On </th>
                        <th colspan="2"><i class="fa fa-cogs" aria-hidden="true"></i> Actions </th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(project,index) in projects">                        
                        <td>@{{project.project_name}}</td>
                        <td>@{{project.project_description}}</td>
                        <td>@{{project.users}}</td>
                        <td>@{{project.api_count}}</td>
                        <td>
                            <span v-if="project.project_status === 1" class="badge badge-danger badge-roundless">Pending</span>
                            <span v-else-if="project.project_status === 2" class="badge badge-info badge-roundless">In-Progress</span>
                            <span v-else class="badge badge-success badge-roundless">Completed</span>
                            </td>
                        <td>@{{project.created_at}}</td>                        
                        <td>
                            <a v-bind:href="'/employee/project/'+project.project_id" class="btn btn-xs blue"><i class="fa fa-random" aria-hidden="true"></i> View API 
                            </a>
                        </td>                        
                    </tr>
                </tbody>
            </table>
            
            <p id="dynamic_pager_demo1">
                <ul class="pagination bootpag">
                    <li data-lp="5" class="prev" v-bind:class="{disabled:currentPage==1}">
                        <a href="#" v-on:click.prevent="getNewProjects(currentPage-1)"><i class="fa fa-angle-left"></i></a>
                    </li>
                    
                    <li data-lp="1" v-bind:class="{disabled:currentPage==n}" v-for="n in lastPage" v-if="n>=currentPage && n<=currentPage+4">
                        <a href="#" v-on:click.prevent="getNewProjects(n)">@{{n}}</a>
                    </li>     

                    <li data-lp="6" class="next disabled" v-if="!(currentPage==lastPage)">
                        <a href="#" v-on:click.prevent=""> ... </a>
                    </li>

                    <li data-lp="5" class="prev" v-if="currentPage!==lastPage">
                        <a href="#" v-on:click.prevent="getNewProjects(lastPage)">@{{lastPage}}</a>
                    </li>
                    <li data-lp="6" class="next" v-bind:class="{disabled:currentPage==lastPage}">
                        <a href="#" v-on:click.prevent="getNewProjects(currentPage+1)"><i class="fa fa-angle-right"></i></a>
                    </li>
                </ul>                
            </p>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        var app = new Vue({
            el:'#employee-list',
            data:{
                projects:[],   
                currentPage:'',
                lastPage:'',
                path:'', 
                jumpPage:'',  
                searchQ:'',                
            },
            watch:{
                searchQ:function(){					
					if(this.searchQ.length >= 2){
						this.getSearch();
					}else if(this.searchQ.length === 0){
						this.getProjects();
					}
				},
                projects:function(){
                    if(this.projects.length === 0){
                        this.getProjects();
                    }
                }
            },
            methods:{
                getProjects(){                    
                    axios.get('{{route('creator.list.projects')}}')
                    .then(response => {
                        console.table(response.data);
                        if(response.data.status === 0)
                        {
                            toastr.error(response.data.msg,'System message');
                        }
                        else
                        {
                            this.projects = response.data.data;                            
                            this.currentPage = response.data.meta.current_page;
                            this.lastPage = response.data.meta.last_page;
                            this.path = response.data.meta.path;
                        }
                    })
                    .catch(error => {
                        toastr.info(error,'System message');
                    })
                },
                getNewProjects(page){                    
                    axios.get(this.path+'?page='+page)
                    .then(response => {
                        if(response.data.status === 0)
                        {
                            toastr.error(response.data.msg,'System message');
                        }
                        else
                        {
                            this.projects = response.data.data;
                            this.currentPage = response.data.meta.current_page;
                            this.lastPage = response.data.meta.last_page;
                            this.path = response.data.meta.path;
                        }
                    })
                    .catch(error => {
                        toastr.info(error,'System message');
                    });                    
                },                                                
            },
            mounted(){   
                this.getProjects();            
            }
        });
    </script>
@endsection

@section('style')
.cursorPointer{
    cursor:disabled;
}
@endsection
