@extends('layout.app')
@section('title','Api List for '.$project->project_name)
@section('content') 
<div class="col-md-12">
<div class="portlet light portlet-fit bg-inverse bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-microphone font-red"></i>
            <span class="caption-subject bold font-red uppercase"> {{$project->project_name}}</span>
            <p class="caption-helper">{{$project->project_description}}</p>
        </div>                
    </div>
    <div class="alert alert-info">            
        Please parse the data to <code>JSON</code> before submitting. It is important to parse the data in <code>JSON</code> else the web server might throw errors.
    </div>
    <div class="portlet-body">
        <div class="timeline  white-bg white-bg">
            <!-- TIMELINE ITEM -->
            @foreach($project->api as $api)
            <div class="timeline-item">
                <div class="timeline-badge">
                    <div class="timeline-icon">
                        <i class="font-green-haze">
                        	@if($api->type === 1)
								GET
                        	@else
								POST
                        	@endif
                        </i>
                    </div>
                </div>
                <div class="timeline-body">
                    <div class="timeline-body-arrow"> </div>
                    <div class="timeline-body-head">
                        <div class="col-md-12">
                            <!-- BEGIN Portlet PORTLET-->
                            <div class="portlet box red">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-link" aria-hidden="true"></i>{{$api->endpoint}} </div>
                                    <div class="actions">
                                        @if(Auth::user()->role == 2)           
                                        <a href="{{route('creator.api.edit',['api_id' => $api->api_id])}}" class="btn yellow">
                                            <i class="fa fa-pencil"></i> Edit </a>

                                        <a class="btn green" data-toggle="modal" href="#delete{{$api->api_id}}">
                                            <i class="fa fa-trash"></i> Delete </a>
                                        @endif
                                        <div class="modal fade bs-modal-sm" id="delete{{$api->api_id}}" tabindex="-1" role="dialog" aria-hidden="true">
			                            <div class="modal-dialog modal-sm">
			                                <div class="modal-content">
			                                    <div class="modal-header">
			                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
			                                        <h4 style="color: black;" class="modal-title">Warning</h4>
			                                    </div>
			                                    <div class="modal-body" style="color: black;"> 
			                                    	<p>Are you sure you wish to delete <strong>"{{$api->description}}"</strong>? 
			                                    	</p>
			                                    </div>
			                                    <div class="modal-footer">
			                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancel</button>
			                                        <a href="{{route('creator.api.delete',['api_id' => $api->api_id ])}}" class="btn red">Delete</a>
			                                    </div>
			                                </div>
			                                <!-- /.modal-content -->
			                            </div>
			                            <!-- /.modal-dialog -->
			                        </div>
			                        <!-- /.modal -->
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <p>Description : <strong>{{$api->description}}</strong></p>
                                    <p>This API was added by <code>{{\App\User::find($api->user_id)->name ?: "User Removed."}}</code> on <strong>{{$api->created_at->toDayDateTimeString()}}</strong> 
										@if($api->updated_at != $api->created_at)
                                    	and it was last updated on <strong>{{$api->updated_at->toDayDateTimeString()}}</strong> by <code>{{ \App\User::find($api->update_user_id)->name ?: "User Removed"}}</code>
                                    	@endif
                                </div>
                            </div>
                            <!-- END Portlet PORTLET-->
                        </div>
                    </div>
                    <div class="timeline-body-content">
                        <div class="col-sm-6 center">Request Parameteres</div>
                        <div class="col-sm-6 center">Response</div>
                        <div class="col-sm-6">
                            <pre style="height: 250px" id="jjson{{$api->api_id}}"></pre>
                        	{{-- <pre><code style="height:250px;" class="html">{{$api->parameters ?: "No parameters provided"}}</code></pre> --}}
                            @if(!empty($api->parameters))
                                <script type="text/javascript">
                                    var jjson{{$api->api_id}} = {!! $api->parameters !!};
                                    $('#jjson{{$api->api_id}}').jsonViewer(jjson{{$api->api_id}});
                                </script>
                            @endif

                        </div>
                        <div class="col-sm-6">
                        	{{-- <pre><code style="height:250px;" class="html">{{$api->response ?: ""}}</code></pre> --}}
                            
                            <pre style="height: 250px" id="response{{$api->api_id}}"></pre>
                            <script type="text/javascript">
                                var response{{$api->api_id}} = {!! $api->response !!};
                                $('#response{{$api->api_id}}').jsonViewer(response{{$api->api_id}});
                            </script>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            <!-- END TIMELINE ITEM -->                       
        </div>
    </div>
</div>
</div>
@endsection
