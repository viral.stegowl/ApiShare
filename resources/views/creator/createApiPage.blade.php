@extends('layout.app')
@section('title','Create new Api')
@section('content') 
<div class="portlet box red" id="employee">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Please fill this form. </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form v-on:submit.prevent="createApi" class="form-horizontal form-bordered form-row-stripped">
            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3">Project </label>
                    <div class="col-md-9">
                        <select class="form-control" v-model="details.project_id">
                        	<option disabled selected>Select a project</option>
                        	@foreach($projects as $project)
							<option v-bind:value="{{$project->project->project_id}}">{{$project->project->project_name}}</option>
                        	@endforeach
                        </select>
                        <small class="help-block"> Please select a project for this API. </small>
                    </div>
                </div> 
                <div class="form-group">
                    <label class="control-label col-md-3">Description</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Enter API description" v-model="details.description">
                        <small class="help-block"> Please enter a short description for this API.</small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Endpoint / URL</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Enter Endpoint/URL" v-model="details.endpoint">
                        <small class="help-block"> Please specify the full Endpoint/URL for this API</small>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">Request Type</label>
                    <div class="col-md-9">
                        <div class="mt-radio-list">
                            <label class="mt-radio">
                                <input type="radio" id="optionsRadios22" value="1"  v-model="details.type"> GET
                                <span></span>
                            </label>
                            <label class="mt-radio">
                                <input type="radio" id="optionsRadios23" value="2" v-model="details.type"> POST
                                <span></span>
                            </label>                            
                        </div>
                        <small class="help-block"> Request type of the endpoint.</small>
                    </div>
                </div>                

                <div class="form-group">
                    <label class="control-label col-md-3">Request Parameteres</label>
                    <div class="col-md-9">
                        <textarea style="height: 100px;" class="form-control" placeholder="Enter request parameters" v-model="details.parameters"></textarea>
                        <small>Please enter the request parameter in <code>JSON</code> format.</small>
                    </div>
                </div>                
                <div class="form-group">
                    <label class="control-label col-md-3">Response</label>
                    <div class="col-md-9">
                        <textarea style="height: 200px;" class="form-control" v-model="details.response" placeholder="Enter response"></textarea>
                        <small class="help-block"> Please enter the response in <code>JSON</code> format.</small>
                    </div>
                </div>                            
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <button v-bind:disabled="ifAttempt" type="submit" class="btn green">
                        <i class="fa fa-check"></i> @{{submitButton}}</button>
                        <a href="{{URL::previous()}}" class="btn default"><i class="fa fa-times"></i> Cancel</a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>            
@endsection

@section('script')
<script type="text/javascript">
        var app = new Vue({
            el:'#employee',
            data:{
                details:{
                    description:'',
                    endpoint:'',
                    type:'',
                    parameters:'',
                    response:'',
                    project_id:'',
                    user_id:'{{Auth::user()->user_id}}'                                    
                },                
                submitButton:'Add',
                ifAttempt:false
            },
            methods:{
            	createApi(){
            		if(this.details.project_id === '')
            		{
            			toastr.error("Please select a project for this API.",'System message');
            		}else{
            			if(this.details.description === ''){
            				toastr.error("Please enter a short description for this API.",'System message');	
            			}else{
            				if(this.details.endpoint === ''){
	            				toastr.error("Please enter an endpoint/url.",'System message');	
	            			}else{
	            				if(this.details.type === ''){
		            				toastr.error("Please select a request type.",'System message');	
		            			}else{
		            				if(this.details.response === ''){
			            				toastr.error("Please enter the response for this API.",'System message');	
			            			}else{
			            				this.submitButton = "Adding...";
			            				this.ifAttempt = true;
			            				axios.post('{{route('creator.api.save')}}',this.details)
			            				.then(response => {
			            					if(response.data.status === 0)
			            					{
			            						toastr.error(response.data.msg,'System message');
			            						this.submitButton = "Add";
			            						this.ifAttempt = false;
			            					}
			            					else
			            					{
			            						toastr.success(response.data.msg,'System message');
			            						this.submitButton = "Added";
			            						this.ifAttempt = false;
			            						setTimeout(function(){
			            							window.location.href = "{{route('creator.main')}}";
			            						},1000);	
			            					}
			            				}) 
			            				.catch(error => {
			            					toastr.error(error,'System message');	
		            						this.submitButton = "Add";
		            						this.ifAttempt = false;
			            				})
			            			}
		            			}
	            			}	
            			}
            		}
            	}       	              
            },
        });
    </script>
@endsection
