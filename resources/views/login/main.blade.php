<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>ApiShare - Login</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="Preview page of Metronic Admin Theme #3 for " name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{asset('/assets/global/plugins/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />        
        <link href="{{asset('/assets/global/plugins/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />        
        <link href="{{asset('/assets/global/css/components.min.css')}}" rel="stylesheet" id="style_components" type="text/css" />        
        <link href="{{asset('/assets/pages/css/login-2.min.css')}}" rel="stylesheet" type="text/css" />                

    <body class=" login">
        <!-- BEGIN LOGO -->
        <div class="logo" style="color: white; font-size: 25px;">            
            <i class="fa fa-file-text" aria-hidden="true"></i> <span style="color: red; font-weight: bold; text-shadow: 0px 0px 2px black;">Api</span> <span>Share</span>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="content" id="app-login">
            <!-- BEGIN LOGIN FORM -->
            
            <form class="login-form" v-on:submit.prevent="checkUser">                
                <div class="form-title">
                    <span class="form-title">Welcome,</span>
                    <span class="form-subtitle">Please login.</span>
                </div> 
                @if(session('success'))  
                <div class="note note-success">                    
                    <p>{{session('success')}}</p>
                </div>             
                @endif
                <div class="form-group">                   
                    <label class="control-label visible-ie8 visible-ie9">Email</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" v-model="detail.email" v-bind:disabled="ifAttempt" /> </div>
                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" v-model="detail.password" v-bind:disabled="ifAttempt" /> </div>
                <div class="form-actions">
                    <button type="submit" v-if="!authSuccess" v-bind:disabled="ifAttempt" class="btn red btn-block uppercase">@{{buttonText}}</button> 
                    <button type="button" class="btn green-haze btn-block uppercase" v-else>@{{buttonText}}</button>
                </div>                                               
            </form>
            <!-- END LOGIN FORM -->                    
        </div> 
        <script src="{{asset('/assets/global/plugins/jquery.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('/assets/global/scripts/vue.js')}}"></script>
        <script src="{{asset('/assets/global/scripts/axios.js')}}"></script>  
        <script src="{{asset('/assets/global/scripts/notify.min.js')}}"></script>  
        <script type="text/javascript">
            var app = new Vue({
                el:'#app-login',
                data:{
                    detail:{
                        email:'',
                        password:'',
                        _token:'{{csrf_token()}}',
                    },
                    ifAttempt:false,
                    buttonText:'login',
                    authSuccess:false
                },
                methods:{                    
                    checkUser(){
                        this.ifAttempt = true;
                        this.buttonText = 'Checking...';
                        axios.post('http://apishare.test/verify',this.detail)
                        .then(response => {
                            console.log(response.data);
                            if(response.data.status == 0){
                                this.buttonText = 'login';
                                if(response.data.data.email) $.notify(response.data.data.email);
                                if(response.data.data.password) $.notify(response.data.data.password);
                                if(!response.data.data) $.notify(response.data.msg);
                            }else{
                                if(response.data.data.role === 1){
                                    window.location.href = '{{route('admin.dashboard')}}';
                                }else{
                                    window.location.href = '{{route('creator.main')}}';
                                }
                                this.buttonText = 'User Authenticated. Redirecting...';
                                this.authSuccess = true;
                            }                            
                            this.ifAttempt = false;
                        })
                        .catch(error => {
                            console.log(error.data);
                            this.ifAttempt = false;
                            this.buttonText = 'login';
                        });
                    },
                }
            });
        </script>
    </body>
</html>
