@extends('layout.app')

@section('title','Assign Projects to Employees')

@section('content') 
<div class="portlet box red" id="employee">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Please fill this form. </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form v-on:submit.prevent="assignUser" class="form-horizontal form-bordered form-row-stripped">
            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3">Select Project</label>
                    <div class="col-md-9">
                        <select class="form-control" v-model="details.project_id">
                            <option disabled value="">Select a project</option>
                            @foreach ( $projects as $project )
                                <option v-bind:value="{{$project->project_id}}">{{$project->project_name}}</option>
                            @endforeach                            
                        </select> 
                        <small class="help-block"> Please select a project to assign.</small>
                    </div>
                </div> 
                <div class="form-group">
                    <label class="control-label col-md-3">Select Employee</label>
                    <div class="col-md-9">
                        <select class="form-control" v-model="details.user_id">
                            <option disabled value="">Select an employee</option>
                            @foreach ( $users as $user )
                                <option v-bind:value="{{$user->user_id}}">{{$user->name}} 
                                    @if($user->role === 2)
                                        (Creator)
                                    @elseif($user->role === 3)
                                        (Consumer)
                                    @endif
                                
                                </option>
                            @endforeach                            
                        </select> 
                        <small class="help-block"> Please select a user to assign.</small>
                    </div>
                </div>                                          
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <button v-bind:disabled="ifAttempt" type="submit" class="btn green">
                        <i class="fa fa-check"></i> @{{submitButton}}</button>
                        <a href="{{URL::previous()}}" class="btn default"><i class="fa fa-times"></i> Cancel</a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>            
@endsection

@section('script')
<script type="text/javascript">
        var app = new Vue({
            el:'#employee',
            data:{
                details:{
                    project_id:'',
                    user_id:'',                                     
                },                
                submitButton:'Assign',
                ifAttempt:false
            },
            methods:{
                assignUser(){
                    if(this.details.project_id === ''){
                        toastr.info("Please select a project.",'System message');
                    }
                    else
                    {
                        if(this.details.user_id === ''){
                            toastr.info("Please select an employee.",'System message');
                        }
                        else
                        {
                            this.submitButton = 'Assigning...';
                            this.ifAttempt = true;
                            axios.post('{{route('admin.assign.user')}}',this.details)
                            .then(response => {
                                if(response.data.status === 0)
                                {
                                    toastr.error(response.data.msg,'System message');
                                    this.submitButton = 'Assign';
                                    this.ifAttempt = false;
                                }
                                else
                                {
                                    toastr.success(response.data.msg,'System message');
                                    this.submitButton = 'Assigned';
                                    this.details.project_id = '';
                                    this.details.user_id = '';
                                    var self = this;
                                    setTimeout(function(){
                                       self.submitButton = 'Assign';
                                       self.ifAttempt = false;
                                    },1000);
                                }
                            })
                            .catch(error => {
                                toastr.error(error,'System message');
                                this.submitButton = 'Assign';
                                this.ifAttempt = false;
                            })
                        }
                    }
                }               
            }
        });
    </script>
@endsection