@extends('layout.app')

@section('title',$user->name.' - Modify Details')

@section('content')    

<div class="portlet box red" id="employee">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Please Use this form to edit records for {{$user->name}}. </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form v-on:submit.prevent="updateUser" class="form-horizontal form-bordered form-row-stripped">
            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3">Name </label>
                    <div class="col-md-9">
                        <input type="text" placeholder="Employee Name" class="form-control" v-model="details.name">
                        <small class="help-block"> Please enter full name of employee </small>
                    </div>
                </div> 
                <div class="form-group">
                    <label class="control-label col-md-3">Email</label>
                    <div class="col-md-9">
                        <input type="text" disabled=true class="form-control" placeholder="Employee Email" v-model="details.email">
                        <small class="help-block"> Please enter employee email. This will become the username. </small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Address</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" v-model="details.address"> 
                        <small class="help-block"> Please enter full address of employee.</small>
                    </div>
                </div>  
                <div class="form-group">
                    <label class="control-label col-md-3">Mobile Number</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" v-model="details.mob_no"> 
                        <small class="help-block"> Please enter valid mobile number of employee. Exactly 10 Digits. Will be used to send SMS notifications.</small>
                    </div>
                </div>             
                <div class="form-group">
                    <label class="control-label col-md-3">Category</label>
                    <div class="col-md-9">
                        <div class="radio-list">
                            <label>
                            <input type="radio" v-model="details.role" name="optionsRadios2" value="2"> Creator </label>
                            <label>
                            <input type="radio" v-model="details.role" name="optionsRadios2" value="3"> Consumer </label>
                        </div>
                        <small class="help-block"> Creator is the employee who creates API and consumer is the employee who will be able to view them. </small>
                    </div>
                </div>                                    
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <button v-bind:disabled="ifAttempt" type="submit" class="btn green">
                        <i class="fa fa-check"></i> @{{submitButton}}</button>
                        <a href="{{URL::previous()}}" class="btn default"><i class="fa fa-times"></i> Cancel</a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>            
@endsection

@section('script')
<script type="text/javascript">
        var app = new Vue({
            el:'#employee',
            data:{
                details:{
                    name:'{{$user->name}}',
                    email:'{{$user->email}}',                    
                    role:'{{$user->role}}',
                    address:'{{$user->employee->address}}',
                    mob_no:'{{$user->employee->mob_no}}',  
                    latitude:'',
                    longitude:''                  
                },                
                submitButton:'Save',
                ifAttempt:false
            },
            methods:{
                updateUser(){                                        
                    if(this.details.name === ''){
                       toastr.info("Please enter employee's full name.",'System message');
                    }else{                                               
                        if(this.details.address === ''){
                            toastr.info("Please enter employee's full address.",'System message');
                        }else{
                            if(this.details.mob_no === '' || this.details.mob_no.match(/[a-zA-Z]/i) || this.details.mob_no.length<=9 || this.details.mob_no.length>=12){
                                toastr.info("Invalid mobile number.",'System message');
                            }else{
                                if(this.details.role === ''){
                                    toastr.info("Please select a category. Based on this, we will distribute features to the employees.",'System message');
                                }else{
                                    this.ifAttempt = true;
                                    this.submitButton = 'Saving...';
                                    axios.get('https://maps.googleapis.com/maps/api/geocode/json?address='+ encodeURI(this.details.address) +'&key=AIzaSyBGVU2zj7p07F1QE_9WTcLXXBafxCVdKj4')
                                    .then(response => {
                                        if(response.data.status==="OK")
                                        {						
                                            this.details.latitude = response.data.results[0].geometry.location.lat
                                            this.details.longitude = response.data.results[0].geometry.location.lng
                                            axios.post('{{route('admin.emp.add')}}/update/{{$user->user_id}}',this.details)
                                            .then(response => {
                                                if(response.data.status === 0){
                                                    toastr.success(response.data.msg,'System message');
                                                    this.ifAttempt = false;
                                                    this.submitButton = "Save";
                                                }else{              
                                                    this.submitButton = "Saved. Redirecting...";
                                                    toastr.success(response.data.msg,'System message');
                                                    setTimeout(function(){
                                                        window.location.href = '{{route('admin.emp.index')}}';
                                                    },1000);
                                                }
                                            })
                                            .catch(error => {
                                                toastr.error(error,'System message');
                                                this.ifAttempt = false;
                                                this.submitButton = 'Save';
                                            })
                                        }  
                                        else{
                                            toastr.info("Please enter a valid address. We could not verify this address.",'System message');
                                            this.ifAttempt = false;
                                            this.submitButton = 'Save';
                                        }                      
                                    }).catch(error => {
                                        toastr.info("Please enter a valid address. We could not verify this address.",'System message');
                                        this.ifAttempt = false;
                                        this.submitButton = 'Save';
                                    });                                                
                                }                                
                            }
                        }
                    }                    
                },                
            }
        });
    </script>
@endsection
