@extends('layout.app')

@section('title','Employee List')

@section('content')
<div class="portlet" id="employee-list">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-users" aria-hidden="true"></i> Employee Details and other options </div> 
            <a href="#" v-on:click.prevent="getNewEmployees(currentPage)" class="btn red-mint btn-outline sbold uppercase pull-right" style="margin-left:5px;"><i class="fa fa-refresh" aria-hidden="true"></i></a>       
            <a href="{{route('admin.emp.addPage')}}" class="btn red-mint btn-outline sbold uppercase pull-right"><i class="fa fa-user-plus" aria-hidden="true"></i> Add Employee</a>
        </div>
    <div class="portlet-title">
        <span class="pull-right">You are currently on page <strong>@{{currentPage}}</strong> of <strong>@{{lastPage}}</strong> pages.</span>

        <div class="col-md-1">            
            <select class="form-control" v-model="jumpPage" v-on:change.prevent="getNewEmployees(jumpPage)">
                <option v-for="n in lastPage" v-bind:disabled="currentPage==n" v-bind:value="n">@{{n}}</option>
            </select>
        </div>

        <div class="col-md-2">            
            <input type="text" v-model="searchQ" class="form-control" placeholder="Search...">
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-scrollable">
            <table class="table table-striped table-bordered table-advance table-hover">
                <thead>
                    <tr>                        
                        <th class="hidden-xs"><i class="fa fa-user"></i> Name </th>
                        <th><i class="fa fa-shopping-cart"></i> Email </th>
                        <th><i class="fa fa-shopping-cart"></i> Last Login </th>
                        <th><i class="fa fa-shopping-cart"></i> Added On </th>
                        <th><i class="fa fa-shopping-cart"></i> Role </th>
                        <th><i class="fa fa-shopping-cart"></i> Status </th>
                        <th colspan="2"><i class="fa fa-cogs" aria-hidden="true"></i> Actions </th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(employee,index) in employees">                        
                        <td>@{{employee.name}}</td>
                        <td>@{{employee.email}}</td>
                        <td>@{{employee.last_login}}</td>
                        <td>@{{employee.created_at}}</td>
                        <td>
                            <span class="badge badge-primary badge-roundless" v-if="employee.role === 2"> Creator </span>
                            <span class="badge badge-info badge-roundless" v-else> Consumer 
                            </span>
                        </td>
                        <td>
                            <a data-toggle="modal" v-bind:href="'#status'+employee.user_id" class="badge badge-success badge-roundless" v-if="employee.status === 1"> Active</a>
                            <a data-toggle="modal" v-bind:href="'#status'+employee.user_id" class="badge badge-danger badge-roundless" v-else>In-Active</a>

                            <div class="modal fade bs-modal-sm" v-bind:id="'status'+employee.user_id" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Warning</h4>
                                    </div>
                                    <div class="modal-body"> <p v-if="employee.status === 1">Are you sure you wish to disable <strong>"@{{employee.name}}"</strong> account? </p> 
                                    <p v-else>Are you sure you wish to enable <strong>"@{{employee.name}}"</strong> account? </p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancel</button>
                                        <button type="button" v-if="employee.status === 1" v-on:click.prevent="changeStatus(employee.user_id)" class="btn red">Disable</button>
                                        <button type="button" v-else v-on:click.prevent="changeStatus(employee.user_id)" class="btn green">Enable</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </td>                        
                        <td>
                            <a v-bind:href="'{{route('admin.emp.index')}}/edit/'+employee.user_id" class="btn btn-xs blue"><i class="fa fa-edit"></i> Edit 
                            </a>
                        </td>
                        <td>
                            <a class="btn btn-xs red" data-toggle="modal" v-bind:href="'#delete'+employee.user_id"> 
                                <i class="fa fa-trash"></i> Delete
                            </a>
                            <div class="modal fade bs-modal-sm" v-bind:id="'delete'+employee.user_id" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Warning</h4>
                                    </div>
                                    <div class="modal-body"> <p>Are you sure you wish to delete <strong>"@{{employee.name}}"</strong>? </p> This will delete all the records for this employee. </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancel</button>
                                        <button type="button" v-on:click.prevent="deleteEmployee(employee.user_id,index)" class="btn red">@{{deleteButton}}</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                        </td>
                    </tr>
                </tbody>
            </table>
            
            <p id="dynamic_pager_demo1">
                <ul class="pagination bootpag">
                    <li data-lp="5" class="prev" v-bind:class="{disabled:currentPage==1}">
                        <a href="#" v-on:click.prevent="getNewEmployees(currentPage-1)"><i class="fa fa-angle-left"></i></a>
                    </li>
                    
                    <li data-lp="1" v-bind:class="{disabled:currentPage==n}" v-for="n in lastPage" v-if="n>=currentPage && n<=currentPage+4">
                        <a href="#" v-on:click.prevent="getNewEmployees(n)">@{{n}}</a>
                    </li>     

                    <li data-lp="6" class="next disabled" v-if="!(currentPage==lastPage)">
                        <a href="#" v-on:click.prevent=""> ... </a>
                    </li>

                    <li data-lp="5" class="prev" v-if="currentPage!==lastPage">
                        <a href="#" v-on:click.prevent="getNewEmployees(lastPage)">@{{lastPage}}</a>
                    </li>
                    <li data-lp="6" class="next" v-bind:class="{disabled:currentPage==lastPage}">
                        <a href="#" v-on:click.prevent="getNewEmployees(currentPage+1)"><i class="fa fa-angle-right"></i></a>
                    </li>
                </ul>                
            </p>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        var app = new Vue({
            el:'#employee-list',
            data:{
                employees:[],   
                currentPage:'',
                lastPage:'',
                path:'', 
                jumpPage:'',  
                searchQ:'',
                deleteButton:'Delete',
            },
            watch:{
                searchQ:function(){					
					if(this.searchQ.length >= 2){
						this.getSearch();
					}else if(this.searchQ.length == 0){
						this.getEmployees();
					}
				},
                employees:function(){
                    if(this.employees.length == 0){
                        this.getEmployees();
                    }
                }
            },
            methods:{
                getEmployees(){                    
                    axios.get('{{route('admin.emp.list')}}')
                    .then(response => {
                        if(response.data.status == 0)
                        {
                            toastr.info(response.data.msg,'System message');
                        }
                        else
                        {
                            this.employees = response.data.data;
                            this.currentPage = response.data.meta.current_page;
                            this.lastPage = response.data.meta.last_page;
                            this.path = response.data.meta.path;                            
                        }
                    })
                    .catch(error => {
                        toastr.info(error,'System message');
                    })
                },
                getNewEmployees(page){                    
                    axios.get(this.path+'?page='+page)
                    .then(response => {
                        if(response.data.status == 0)
                        {
                            toastr.info(response.data.msg,'System message');
                        }
                        else
                        {
                            this.employees = response.data.data;
                            this.currentPage = response.data.meta.current_page;
                            this.lastPage = response.data.meta.last_page;
                            this.path = response.data.meta.path;
                        }
                    })
                    .catch(error => {
                        toastr.info(error,'System message');
                    });                    
                },
                deleteEmployee(user_id,index){
                    this.deleteButton = "Deleting...";
                    axios.delete('{{route('admin.emp.index')}}/delete/'+user_id)
                    .then(response => { 
                        $('#delete'+user_id).modal('hide');                        
                        this.deleteButton = 'Delete';
                        var self = this; 
                        setTimeout(function(){
                            self.employees.splice(index,1);
                        },500);
                        //this.employees.splice(index,1);                        
                    })
                    .catch(error => {
                        toastr.error(error,'System Message');
                    });
                },
                getSearch: _.debounce(function(){
                    axios.get('{{route('admin.emp.index')}}/search/'+this.searchQ)
                    .then(response => {
                        if(response.data.status == 0)
                        {
                            toastr.info(response.data.msg,'System message');
                        }
                        else
                        {
                            this.employees = response.data.data;
                            this.currentPage = response.data.meta.current_page;
                            this.lastPage = response.data.meta.last_page;
                            this.path = response.data.meta.path;
                        }                        
                    })
                    .catch(error => {
                        toastr.info(error,'System message');
                    })
                },500),
                changeStatus(user_id){
                    axios.get('{{route('admin.emp.index')}}/changeStatus/'+user_id)
                    .then(response => {
                        if(response.data.status === 0){                            
                            toastr.error(response.data.msg,'System message');
                        }
                        else{
                            this.getNewEmployees(this.currentPage);
                        }
                        $('#status'+user_id).modal('hide');
                    })
                    .catch(error => {
                        toastr.info(error,'System message');
                        $('#status'+user_id).modal('hide');
                    })
                }
            },
            mounted(){
                this.getEmployees();
            }
        });
    </script>
@endsection

@section('style')
.cursorPointer{
    cursor:disabled;
}
@endsection
