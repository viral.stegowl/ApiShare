@extends('layout.app')

@section('title','Projects List')

@section('content')
<div class="portlet" id="employee-list">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-users" aria-hidden="true"></i> Project Details and other options </div> 
            <a href="#" v-on:click.prevent="getNewProjects(currentPage)" class="btn red-mint btn-outline sbold uppercase pull-right" style="margin-left:5px;"><i class="fa fa-refresh" aria-hidden="true"></i></a>       
            <a href="{{route('admin.projects.addPage')}}" class="btn red-mint btn-outline sbold uppercase pull-right"><i class="fa fa-user-plus" aria-hidden="true"></i> Add Project</a>
        </div>
    <div class="portlet-title">
        <span class="pull-right">You are currently on page <strong>@{{currentPage}}</strong> of <strong>@{{lastPage}}</strong> pages.</span>

        <div class="col-md-1">            
            <select class="form-control" v-model="jumpPage" v-on:change.prevent="getNewProjects(jumpPage)">
                <option v-for="n in lastPage" v-bind:disabled="currentPage==n" v-bind:value="n">@{{n}}</option>
            </select>
        </div>

        <div class="col-md-2">            
            <input type="text" v-model="searchQ" class="form-control" placeholder="Search...">
        </div>
    </div>
    <div class="portlet-body">
        <div class="table-scrollable">
            <table class="table table-striped table-bordered table-advance table-hover">
                <thead>
                    <tr>                        
                        <th class="hidden-xs"><i class="fa fa-user"></i> Project Name </th>
                        <th><i class="fa fa-shopping-cart"></i> Project Description </th>
                        <th><i class="fa fa-shopping-cart"></i> Project Status </th>
                        <th><i class="fa fa-shopping-cart"></i> Added On </th>
                        <th colspan="2"><i class="fa fa-cogs" aria-hidden="true"></i> Actions </th>
                    </tr>
                </thead>
                <tbody>
                    <tr v-for="(project,index) in projects">                        
                        <td>@{{project.project_name}}</td>
                        <td>@{{project.project_description}}</td>
                        <td>
                            <span v-if="project.project_status === 1" class="badge badge-danger badge-roundless">Pending</span>
                            <span v-else-if="project.project_status === 2" class="badge badge-info badge-roundless">In-Progress</span>
                            <span v-else class="badge badge-success badge-roundless">Completed</span>
                            </td>
                        <td>@{{project.created_at}}</td>                        
                        <td>
                            <a v-bind:href="'{{route('admin.projects.index')}}/edit/'+project.project_id" class="btn btn-xs blue">                     <i class="fa fa-edit"></i> Edit 
                            </a>
                        </td>
                        <td>
                            <a class="btn btn-xs red" data-toggle="modal" v-bind:href="'#delete'+project.project_id"> 
                                <i class="fa fa-trash"></i> Delete
                            </a>
                            <div class="modal fade bs-modal-sm" v-bind:id="'delete'+project.project_id" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-sm">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Warning</h4>
                                    </div>
                                    <div class="modal-body"> <p>Are you sure you wish to delete <strong>"@{{project.project_name}}"</strong>? </p> This will delete all the records for this employee. </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn dark btn-outline" data-dismiss="modal">Cancel</button>
                                        <button type="button" v-on:click.prevent="deleteEmployee(project.project_id,index)" class="btn red">@{{deleteButton}}</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                        <!-- /.modal -->
                        </td>
                    </tr>
                </tbody>
            </table>
            
            <p id="dynamic_pager_demo1">
                <ul class="pagination bootpag">
                    <li data-lp="5" class="prev" v-bind:class="{disabled:currentPage==1}">
                        <a href="#" v-on:click.prevent="getNewProjects(currentPage-1)"><i class="fa fa-angle-left"></i></a>
                    </li>
                    
                    <li data-lp="1" v-bind:class="{disabled:currentPage==n}" v-for="n in lastPage" v-if="n>=currentPage && n<=currentPage+4">
                        <a href="#" v-on:click.prevent="getNewProjects(n)">@{{n}}</a>
                    </li>     

                    <li data-lp="6" class="next disabled" v-if="!(currentPage==lastPage)">
                        <a href="#" v-on:click.prevent=""> ... </a>
                    </li>

                    <li data-lp="5" class="prev" v-if="currentPage!==lastPage">
                        <a href="#" v-on:click.prevent="getNewProjects(lastPage)">@{{lastPage}}</a>
                    </li>
                    <li data-lp="6" class="next" v-bind:class="{disabled:currentPage==lastPage}">
                        <a href="#" v-on:click.prevent="getNewProjects(currentPage+1)"><i class="fa fa-angle-right"></i></a>
                    </li>
                </ul>                
            </p>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script type="text/javascript">
        var app = new Vue({
            el:'#employee-list',
            data:{
                projects:[],   
                currentPage:'',
                lastPage:'',
                path:'', 
                jumpPage:'',  
                searchQ:'',
                deleteButton:'Delete',
            },
            watch:{
                searchQ:function(){					
					if(this.searchQ.length >= 2){
						this.getSearch();
					}else if(this.searchQ.length === 0){
						this.getProjects();
					}
				},
                projects:function(){
                    if(this.projects.length === 0){
                        this.getProjects();
                    }
                }
            },
            methods:{
                getProjects(){                    
                    axios.get('{{route('admin.projects.list')}}')
                    .then(response => {
                        if(response.data.status === 0)
                        {
                            toastr.info(response.data.msg,'System message');
                        }
                        else
                        {
                            this.projects = response.data.data;
                            this.currentPage = response.data.meta.current_page;
                            this.lastPage = response.data.meta.last_page;
                            this.path = response.data.meta.path;
                        }
                    })
                    .catch(error => {
                        toastr.info(error,'System message');
                    })
                },
                getNewProjects(page){                    
                    axios.get(this.path+'?page='+page)
                    .then(response => {
                        if(response.data.status === 0)
                        {
                            toastr.info(response.data.msg,'System message');
                        }
                        else
                        {
                            this.projects = response.data.data;
                            this.currentPage = response.data.meta.current_page;
                            this.lastPage = response.data.meta.last_page;
                            this.path = response.data.meta.path;
                        }
                    })
                    .catch(error => {
                        toastr.info(error,'System message');
                    });                    
                },
                deleteEmployee(project_id,index){
                    this.deleteButton = "Deleting...";
                    axios.delete('{{route('admin.projects.index')}}/delete/'+project_id)
                    .then(response => { 
                        $('#delete'+project_id).modal('hide');                        
                        this.deleteButton = 'Delete';
                        var self = this; 
                        setTimeout(function(){
                            self.projects.splice(index,1);
                        },500);
                        //this.projects.splice(index,1);                        
                    })
                    .catch(error => {
                        toastr.error(error,'System Message');
                    });
                },
                getSearch: _.debounce(function(){
                    axios.get('{{route('admin.projects.index')}}/search/'+this.searchQ)
                    .then(response => {
                        if(response.data.status === 0)
                        {
                            toastr.info(response.data.msg,'System message');
                        }
                        else
                        {
                            this.projects = response.data.data;
                            this.currentPage = response.data.meta.current_page;
                            this.lastPage = response.data.meta.last_page;
                            this.path = response.data.meta.path;                            
                        }
                    })
                    .catch(error => {
                        toastr.info(error,'System message');
                    })
                },500),                
            },
            mounted(){
                this.getProjects();
            }
        });
    </script>
@endsection

@section('style')
.cursorPointer{
    cursor:disabled;
}
@endsection
