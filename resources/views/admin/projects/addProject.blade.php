@extends('layout.app')

@section('title','Add New Project')

@section('content') 
<div class="portlet box red" id="employee">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Please fill this form. </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form v-on:submit.prevent="createProject" class="form-horizontal form-bordered form-row-stripped">
            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3">Project Name </label>
                    <div class="col-md-9">
                        <input type="text" placeholder="Project Name" class="form-control" v-model="details.project_name">
                        <small class="help-block"> Please enter full project name</small>
                    </div>
                </div> 
                <div class="form-group">
                    <label class="control-label col-md-3">Project Description</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Project description" v-model="details.project_description">
                        <small class="help-block"> Please enter briefly about this project</small>
                    </div>
                </div>                    
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <button v-bind:disabled="ifAttempt" type="submit" class="btn green">
                        <i class="fa fa-check"></i> @{{submitButton}}</button>
                        <a href="{{URL::previous()}}" class="btn default"><i class="fa fa-times"></i> Cancel</a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>            
@endsection

@section('script')
<script type="text/javascript">
        var app = new Vue({
            el:'#employee',
            data:{
                details:{
                    project_name:'',
                    project_description:'',                    
                },                
                submitButton:'Add',
                ifAttempt:false
            },
            methods:{
                createProject(){                                        
                    if(this.details.project_name === ''){
                        toastr.info("Please enter Project name.",'System message');
                    }
                    else
                    {
                        if(this.details.project_description === ''){
                            toastr.info("Please enter project description.",'System message');
                        }
                        else
                        {                            
                            axios.post('{{route('admin.projects.add')}}',this.details)
                            .then(response => {
                                if(response.data.status === 0){
                                    toastr.success(response.data.msg,'System message');
                                    this.ifAttempt = false;
                                    this.submitButton = "Add";
                                }else{              
                                    this.submitButton = "Added. Redirecting...";
                                    toastr.success(response.data.msg,'System message');
                                    setTimeout(function(){
                                        window.location.href = '{{route('admin.projects.index')}}';
                                    },1000);
                                }
                            })
                            .catch(error => {
                                toastr.error(error,'System message');
                            })
                        }             
                    }                
                },
            }            
        });
    </script>
@endsection
