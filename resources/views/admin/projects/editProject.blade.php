@extends('layout.app')

@section('title','Edit Project '.$project->project_name)

@section('content') 
<div class="portlet box red" id="employee">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Please fill this form for {{$project->project_name}} </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form v-on:submit.prevent="updateProject" class="form-horizontal form-bordered form-row-stripped">
            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3">Project Name </label>
                    <div class="col-md-9">
                        <input type="text" placeholder="Project Name" class="form-control" v-model="details.project_name">
                        <small class="help-block"> Please enter full project name</small>
                    </div>
                </div> 
                <div class="form-group">
                    <label class="control-label col-md-3">Project Description</label>
                    <div class="col-md-9">
                        <input type="text" class="form-control" placeholder="Project description" v-model="details.project_description">
                        <small class="help-block"> Please enter briefly about this project</small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Project Status</label>
                    <div class="col-md-9">
                        <div class="mt-radio-list">
                            <label class="mt-radio">
                                <input type="radio" id="optionsRadios22" value="1"  v-model="details.project_status"> Pending
                                <span></span>
                            </label>
                            <label class="mt-radio">
                                <input type="radio" id="optionsRadios23" value="2" v-model="details.project_status"> In Progress
                                <span></span>
                            </label>
                            <label class="mt-radio">
                                <input type="radio" id="optionsRadios24" value="3" v-model="details.project_status"> Completed
                                <span></span>
                            </label>
                        </div>
                        <small class="help-block"> Update the status if it has changed.</small>
                    </div>
                </div>                                   
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <button v-bind:disabled="ifAttempt" type="submit" class="btn green">
                        <i class="fa fa-check"></i> @{{submitButton}}</button>
                        <a href="{{URL::previous()}}" class="btn default"><i class="fa fa-times"></i> Cancel</a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>            
@endsection

@section('script')
<script type="text/javascript">
        var app = new Vue({
            el:'#employee',
            data:{
                details:{
                    project_id:'{{$project->project_id}}',
                    project_name:'{{$project->project_name}}',
                    project_description:'{{$project->project_description}}',                
                    project_status:'{{$project->project_status}}'
                },                
                submitButton:'Update',
                ifAttempt:false
            },
            methods:{
                updateProject(){                                        
                    if(this.details.project_name === ''){
                        toastr.info("Please enter Project name.",'System message');
                    }
                    else
                    {
                        if(this.details.project_description === ''){
                            toastr.info("Please enter project description.",'System message');
                        }
                        else
                        {                            
                            axios.post('{{route('admin.projects.index')}}/update/'+this.details.project_id,this.details)
                            .then(response => {
                                if(response.data.status === 0){
                                    toastr.success(response.data.msg,'System message');
                                    this.ifAttempt = false;
                                    this.submitButton = "Update";
                                }else{              
                                    this.submitButton = "Updated. Redirecting...";
                                    toastr.success(response.data.msg,'System message');
                                    setTimeout(function(){
                                        window.location.href = '{{route('admin.projects.index')}}';
                                    },1000);                                    
                                }
                            })
                            .catch(error => {
                                toastr.error(error,'System message');
                                this.ifAttempt = false;
                                this.submitButton = "Update";
                            })
                        }             
                    }                
                },
            }            
        });
    </script>
@endsection