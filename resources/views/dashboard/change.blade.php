@extends('layout.app')

@section('title','Change your password')

@section('content')
	<div class="portlet box red" id="employee">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Please fill this form. </div>
    </div>
    <div class="portlet-body form">
        <!-- BEGIN FORM-->
        <form v-on:submit.prevent="createProject" class="form-horizontal form-bordered form-row-stripped">
            <div class="form-body">
                <div class="form-group">
                    <label class="control-label col-md-3">Current Password </label>
                    <div class="col-md-9">
                        <input type="password" placeholder="Current Password" class="form-control" v-model="details.password">
                        <small class="help-block"> Please enter your current password</small>
                    </div>
                </div> 
                <div class="form-group">
                    <label class="control-label col-md-3">New Password </label>
                    <div class="col-md-9">
                        <input type="password" placeholder="New Password" class="form-control" v-model="details.new_password">
                        <small class="help-block"> Please enter a new password</small>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Confirm New Password </label>
                    <div class="col-md-9">
                        <input type="password" placeholder="Confirm New Password" class="form-control" v-model="details.c_password">
                        <small class="help-block"> Please enter your new password again.</small>
                    </div>
                </div>                    
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <button v-bind:disabled="ifAttempt" type="submit" class="btn green">
                        <i class="fa fa-check"></i> @{{submitButton}}</button>
                        <a href="{{URL::previous()}}" class="btn default"><i class="fa fa-times"></i> Cancel</a>
                    </div>
                </div>
            </div>
        </form>
        <!-- END FORM-->
    </div>
</div>  
@endsection

@section('script')
<script type="text/javascript">
        var app = new Vue({
            el:'#employee',
            data:{
                details:{
                    password:'',
                    new_password:'',                    
                    c_password:'',
                },                
                submitButton:'Change',
                ifAttempt:false
            },
            methods:{
                createProject(){                                        
                    if(this.details.password === ''){
                        toastr.info("Please enter Project name.",'System message');
                    }
                    else
                    {
                        if(this.details.new_password === ''){
                            toastr.info("Please enter project description.",'System message');
                        }
                        else
                        {    
                        	if(this.details.new_password !== this.details.c_password)
                        	{
                        		toastr.info("Your new password does not match your confirm password",'System message');
                        	}       
                        	else
                        	{
                        		axios.post('{{route('update.password')}}',this.details)
	                            .then(response => {
	                                if(response.data.status === 0){
	                                    toastr.error(response.data.msg,'System message');
	                                    this.ifAttempt = false;
	                                    this.submitButton = "Change";
	                                }else{              
	                                    this.submitButton = "Changed";
	                                    toastr.success(response.data.msg,'System message');
	                                    var self = this;
	                                    setTimeout(function(){
	                                        self.submitButton = "Change"
	                                        self.ifAttempt = false;
	                                        window.location.href = "{{route('login.form')}}";
	                                    },2000);
	                                }
	                            })
	                            .catch(error => {
	                                toastr.error(error,'System message');
	                                this.ifAttempt = false;
	                                this.submitButton = "Change";
	                            })
                        	} 	
                        }             
                    }                
                },
            }            
        });
    </script>
@endsection
