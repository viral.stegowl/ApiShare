@extends('layout.app')

@section('title','Dashboard')

@section('content')
<div class="row">
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-green-sharp">
                        <span data-counter="counterup" data-value="7800">{{$projects}}</span>                       
                    </h3>
                    <small>Total Projects</small>
                </div>
                <div class="icon">
                    <i class="icon-pie-chart"></i>
                </div>
            </div>            
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-red-haze">
                        <span data-counter="counterup" data-value="1349">{{$users}}</span>
                    </h3>
                    <small>Total Employees</small>
                </div>
                <div class="icon">
                    <i class="icon-like"></i>
                </div>
            </div>
            
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-blue-sharp">
                        <span data-counter="counterup" data-value="567">{{$pending}}</span>
                    </h3>
                    <small>Projects Pending</small>
                </div>
                <div class="icon">
                    <i class="icon-basket"></i>
                </div>
            </div>            
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-purple-soft">
                        <span data-counter="counterup" data-value="276">{{$completed}}</span>
                    </h3>
                    <small>Projects Completed</small>
                </div>
                <div class="icon">
                    <i class="icon-user"></i>
                </div>
            </div>            
        </div>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="dashboard-stat2 ">
            <div class="display">
                <div class="number">
                    <h3 class="font-purple-soft">
                        <span data-counter="counterup" data-value="276">{{$inprogress}}</span>
                    </h3>
                    <small>Projects In-Progress</small>
                </div>
                <div class="icon">
                    <i class="icon-user"></i>
                </div>
            </div>            
        </div>
    </div>
</div>
@endsection

@section('script')

@endsection