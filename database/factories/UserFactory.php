<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
        'role' => rand(2,3),
        'last_login' => date("Y-m-d H:i:s")
    ];
});

$factory->define(App\Employee::class, function (Faker $faker){
    return [
        'user_id' => function(){
            return factory(App\User::class)->create()->user_id;
        },  
        'address' => $faker->address,
        'mob_no' => mt_rand(1,9),
        'latitude' => $faker->latitude($min = -90, $max = 90),
        'longitude' => $faker->longitude($min = -180, $max = 180)
    ];
});

$factory->define(App\Project::class, function(Faker $faker){
    return [
        'project_name' => $faker->text(20),
        'project_description' => $faker->text(100),
        'project_status' => rand(1,3)
    ];
});