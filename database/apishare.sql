-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2018 at 02:08 PM
-- Server version: 10.1.24-MariaDB
-- PHP Version: 7.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `apishare`
--

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `emp_id` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `address` varchar(500) NOT NULL,
  `mob_no` bigint(20) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`emp_id`, `user_id`, `address`, `mob_no`, `latitude`, `longitude`, `created_at`, `updated_at`) VALUES
(1, 1063, 'samudra complex', 9408758653, 23.0312693, 72.5606418, '2018-01-29 11:51:54', '2018-01-29 11:51:54'),
(2, 1064, 'samudra complex', 9913193001, 23.0312693, 72.5606418, '2018-01-29 12:02:14', '2018-01-29 12:02:14');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE `projects` (
  `project_id` int(11) NOT NULL,
  `project_name` varchar(500) DEFAULT NULL,
  `project_description` varchar(1000) DEFAULT NULL,
  `project_status` int(11) NOT NULL DEFAULT '1' COMMENT '1 = Pending, 2 = in progress, 3 = completed',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`project_id`, `project_name`, `project_description`, `project_status`, `created_at`, `updated_at`) VALUES
(510, 'TheWificonnect', 'TheWificonnect is a revised version of perimeter transforming the presentation and business layer with new design and system design.', 1, '2018-01-29 11:51:21', '2018-01-29 11:51:21');

-- --------------------------------------------------------

--
-- Table structure for table `project_api`
--

CREATE TABLE `project_api` (
  `api_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `endpoint` varchar(500) DEFAULT NULL,
  `type` int(11) DEFAULT NULL COMMENT '1 = GET, 2 = POST',
  `parameters` text,
  `response` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `description` text,
  `update_user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_api`
--

INSERT INTO `project_api` (`api_id`, `project_id`, `user_id`, `endpoint`, `type`, `parameters`, `response`, `created_at`, `updated_at`, `description`, `update_user_id`) VALUES
(8, 510, 1063, 'http://apishare.test/employee/list', 1, NULL, '{\"status\":1,\"msg\":\"List of projects\",\"data\":[{\"project_id\":510,\"project_name\":\"TheWificonnect\",\"project_description\":\"TheWificonnect is a revised version of perimeter t...\",\"api_count\":0,\"project_status\":1,\"created_at\":\"Mon, Jan 29, 2018 11:51 AM\",\"users\":\"Viral Patel\"}],\"meta\":{\"current_page\":1,\"last_page\":1,\"path\":\"http:\\/\\/apishare.test\\/employee\\/list\"}}', '2018-01-29 11:53:21', '2018-01-29 11:53:21', 'Project List for user.', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `project_user`
--

CREATE TABLE `project_user` (
  `assign_id` int(11) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  `user_id` int(11) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_user`
--

INSERT INTO `project_user` (`assign_id`, `project_id`, `user_id`, `created_at`, `updated_at`) VALUES
(11, 510, 1063, '2018-01-29 11:52:04', '2018-01-29 11:52:04'),
(12, 510, 1064, '2018-01-29 12:02:43', '2018-01-29 12:02:43');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL COMMENT '1 = admin, 2 = creator, 3= consumer',
  `status` int(11) NOT NULL DEFAULT '1',
  `last_login` datetime DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `name`, `email`, `password`, `role`, `status`, `last_login`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Super Admin', 'admin@admin.com', '$2y$10$EhgazblKnWfKd2Z7kfW2z.Pvyj6y9.Emh2VrxcRE6bBt2UNa5epDu', 1, 1, '2018-01-29 12:02:38', 'PqatPNPHOVmb6XxVxNYDVtx7d75oBWVKbJh4alcxvNjL0AWktCOGZZD5DS6H', '2018-01-18 05:02:19', '2018-01-29 06:32:38'),
(1063, 'Viral Patel', 'viral.stegowl@gmail.com', '$2y$10$J.rQNA8spLgy0xdg1edYwuhQaGG.h.Uc4RBhdaU62Y41xGqCZYqya', 2, 1, '2018-01-29 11:52:22', '16zUh8vJLjrRcRXibJMca9d4SwEw6F5rJfXhDBoHs98eRKCp9WXAkiOS3puI', '2018-01-29 06:21:54', '2018-01-29 06:22:22'),
(1064, 'Sulay Panchal', 'sulay.stegowl@gmail.com', '$2y$10$8rtIEXbuDcp/95NkV1HppOyB2YcUTuTvjNri48fRpNOgFoaN9XCV2', 3, 1, '2018-01-29 12:02:25', 'wFOYcqwHJlRCtesvTuWPbv6KMLJ85cg2SgdbqdqYRxBCJwRR5nwWEqgxAbBx', '2018-01-29 06:32:14', '2018-01-29 06:32:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`emp_id`),
  ADD KEY `remove user profile` (`user_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `project_api`
--
ALTER TABLE `project_api`
  ADD PRIMARY KEY (`api_id`),
  ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `project_user`
--
ALTER TABLE `project_user`
  ADD PRIMARY KEY (`assign_id`),
  ADD KEY `remove project relation` (`project_id`),
  ADD KEY `remove user relation` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `name` (`name`),
  ADD KEY `email` (`email`),
  ADD KEY `email_2` (`email`,`password`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `emp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=511;
--
-- AUTO_INCREMENT for table `project_api`
--
ALTER TABLE `project_api`
  MODIFY `api_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `project_user`
--
ALTER TABLE `project_user`
  MODIFY `assign_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1065;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `remove user profile` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE;

--
-- Constraints for table `project_api`
--
ALTER TABLE `project_api`
  ADD CONSTRAINT `delete api project relation` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`) ON DELETE CASCADE;

--
-- Constraints for table `project_user`
--
ALTER TABLE `project_user`
  ADD CONSTRAINT `remove project relation` FOREIGN KEY (`project_id`) REFERENCES `projects` (`project_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `remove user relation` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
