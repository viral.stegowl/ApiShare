# ApiShare

Sharing API's between creators and consumers

## Getting Started

ApiShare is a personal project created for a professional reason to make the corporate life easy for employee of 2 or more departments. It mainly focuses on sharing API's documentation between users where the creator has a sleek interface of adding API's and the consumer has options to view the API's with full documentation.

### Prerequisites

There are none for this project.

## Versioning

We manually manage the versioning using an internal system at our company STEGOWL.

## Authors

* **Viral Patel** - *Complete work*

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

