<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class onlyConsumer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->role === 3)
        {
            return $next($request);
        }
        else
        {
            Auth::logout();
            return redirect()->route('login.form')->with('error','Access Denied');
        }
    }
}
