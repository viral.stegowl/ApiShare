<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if(Auth::check()){
                if(Auth::user()->role === 1){
                    return redirect()->route('admin.dashboard');
                }elseif(Auth::user()->role === 2){
                    return redirect()->route('creator.main');
                }else{
                    Auth::logout();
                    return redirect()->route('login.form')->with('success','System still under development');
                }
            }else{
                return redirect()->route('login.form')->with('success','Please login again.');
            }            
        }
        return $next($request);
    }
}
