<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ProjectResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return [
            'project_id' => $this->project_id ?: "",
            'project_name' => $this->project_name ?: "",
            'project_description' => substr($this->project_description,0,50)."..." ?: "",
            'project_status' => $this->project_status ?: "",
            'created_at' => $this->created_at->toDayDateTimeString() ?: ""
        ];
    }
}
