<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class EmployeeResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        //return parent::toArray($request);

        return [
            'user_id' => $this->user_id,
            'name' => $this->name ?: "",
            'email' => $this->email ?: "",
            'role' => $this->role ?: "",
            'status' => $this->status ?: "",
            'last_login' => $this->last_login->toDayDateTimeString() ?: "",
            'created_at' => $this->created_at->toDayDateTimeString() ?: ""
        ];
    }
}
