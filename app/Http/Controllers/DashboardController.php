<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Project;
use Hash;
use Auth;

class DashboardController extends Controller
{
    public function dashboardIndex()
    {
        $data = [
            'users' => User::where('role','!=',1)->count(),
            'projects' => Project::count(),
            'pending' => Project::where(['project_status' => 1])->count(),
            'completed' => Project::where(['project_status' => 3])->count(),
            'inprogress' => Project::where(['project_status' => 2])->count(),
        ];
        return view('dashboard.default')->with($data);
    }

    public function changePassword()
    {
        return view('dashboard.change');
    }

    public function updatePassword(Request $request)
    {
        try{
            $user = User::find(Auth::user()->user_id);
            if(count($user)>0)
            {
                if(Hash::check($request->password, $user->password))
                {
                    $user->password = Hash::make($request->new_password);
                    $user->save();

                    Auth::logout();

                    $response = [
                        'status' => 1,
                        'msg' => 'Password updated successfully.',
                    ];
                }
                else
                {
                    $response = [
                        'status' => 0,
                        'msg' => 'Your old password does not match our records. Please try again.'
                    ];
                }
            }
            else
            {
                $response = [
                    'status' => 0,
                    'msg' => 'Invalid User. Please try again.'
                ]; 
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'status' => 0,
                'msg' => $e->getMessage()
            ];
        }
        return $response;
    }
}
