<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Project;
use App\ProjectUser;

class ProjectUserController extends Controller
{
    public function assignPage()
    {
        $data = [
            'users' => User::where('user_id','!=',1)->get(),
            'projects' => Project::orderBy('project_id','desc')->get()
        ];
        if(count($data['projects'])>0)     
        {
            if(count($data['users'])>0)
            {
                return view('admin.assign.main')->with($data);        
            }
            else
            {
                return redirect()->route('admin.emp.addPage')->with('error','Please add an employee first to assign a project.');
            }
        }
        else
        {   
            return redirect()->route('admin.projects.addPage')->with('error','Please add a project first to assign to an employee.');
        }
        
    }
    public function assignUser(Request $request)
    {
        try{
            if(!empty($request->project_id) && !empty($request->user_id))
            {
                $checkRelation = ProjectUser::where(['project_id' => $request->project_id,'user_id' => $request->user_id])->count();

                if($checkRelation>0)
                {
                    $response = [
                        'status' => 0,
                        'msg' => 'This employee is already assigned to this project. You cannot re-assign the same employee again.'
                    ];
                }
                else
                {
                    ProjectUser::create($request->all());
                    $response = [
                        'status' => 1,
                        'msg' => 'Employee successfully assigned to the project.'
                    ];
                }                
            }
            else
            {
                $response = [
                    'status' => 0,
                    'msg' => 'Invalid arguments given. Please try again.'
                ];
            }
        }
        catch(\Exception $e)
        {
             $response = [
                'status' => 0,
                'msg' => $e->getMessage()." on line ".$e->getLine()." in file ".$e->getFile(),
                'data' => ""
            ];
        }
        return $response;
    }
}
