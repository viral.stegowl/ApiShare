<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Project;
use App\Http\Resources\ProjectResource;
use App\ProjectUser;
use App\ProjectApi;
use Auth;

class ProjectController extends Controller
{
    public function indexPage()
    {
        return view('admin.projects.main');
    }

    public function addProjectPage()
    {
        return view('admin.projects.addProject');
    }

    public function listProjects()
    {
        try{
            $projects = Project::orderBy('project_id','desc')->paginate(10);
            if(count($projects)>0)
            {
                $response = ProjectResource::collection($projects);
            }
            else{
                $response = [
                    'status' => 0,
                    'msg' => 'No projects found. Please add a project.',
                    'data' => ''
                ];
            }
        }catch(\Exception $e){            
            $response = [
                'status' => 0,
                'msg' => $e->getMessage()." on line ".$e->getLine()." in file ".$e->getFile(),
                'data' => ""
            ];
        }
        return $response;
    }


    public function searchProjects($q = null)
    {
        try
        {
            if($q)
            {
                $projects = Project::where('project_name','like','%'.$q.'%')->orderBy('project_id','desc')->paginate(10);
                return ProjectResource::collection($projects);
            }
            else
            {
                $response = [
                    'status' => 0,
                    'msg' => 'Please enter a longer search term.'
                ];
            }   
        }
        catch(\Exception $e)
        {
            $response = [
                'status' => 0,
                'msg' => $e->getMessage()." on line ".$e->getLine()." in file ".$e->getFile(),
                'data' => ""
            ];
        }
        return $response;
    }

    public function addProject(Request $request)
    {
        try{
            Project::create($request->all());
            $response = [
                'status' => 1,
                'msg' => 'Project added.',
                'data' => ''
            ];
        }catch(\Exception $e){
            $response = [
                'status' => 0,
                'msg' => $e->getMessage()." on line ".$e->getLine()." in file ".$e->getFile(),
                'data' => ""
            ];
        }
        return $response;
    }

    public function editProject($project_id = NULL)
    {
        $project = Project::find($project_id);
        
        return view('admin.projects.editProject')->with('project',$project);
    }

    public function updateProject(Request $request,$id = null)
    {
        try{
            if($id)
            {
                Project::where(['project_id' => $id])->update($request->all());

                $response = [
                    'status' => 1,
                    'msg' => 'Project record updated successfully.'
                ];
            }
            else
            {
                $response = [
                    'status' => 0,
                    'msg' => 'Invalid Arguments given. Please try again.',
                    'data' => ''
                ];
            }
        }   
        catch(\Exception $e)
        {
            $response = [
                'status' => 0,
                'msg' => $e->getMessage()." on line ".$e->getLine()." in file ".$e->getFile(),
                'data' => ""
            ];
        }
        return $response;
    }

    public function deleteProject($project_id = NULL)
    {
        try
        {
            $project = Project::find($project_id)->delete();
            if($project){
                $response = [
                    'status' => 1,
                    'msg' => 'Project deleted successfully.',
                    'data' => ''
                ];
            }
            else{
                $response = [
                    'status' => 0,
                    'msg' => 'Record was not deleted. Please try again.',
                    'data' => ''
                ];
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'status' => 0,
                'msg' => $e->getMessage()." on line ".$e->getLine()." in file ".$e->getFile(),
                'data' => ""
            ];
        }
        return $response;        
    }



    /*
    * Creator Functions here on
    * Protected with onlyCreator Middleware
    */

    public function listCreatorProjects()
    {
        return view('creator.projectList');    
    }

    public function getCreatorsProjects()
    {
        try{
            $projects = ProjectUser::with('project')->where(['user_id' => Auth::user()->user_id])->paginate(10);

            if(count($projects)>0)
            {       
                $temp = [];
                foreach($projects as $project)
                {
                    $arr['project_id'] = $project->project->project_id ?: '';
                    $arr['project_name'] = $project->project->project_name ?: '';
                    $arr['project_description'] = substr($project->project->project_description,0,50)."..." ?: '';
                    $usernames['users'] = [];
                    $users = ProjectUser::with('user')->where(['project_id' => $project->project_id])->get();
                    if(count($users)>0)
                    {
                        foreach($users as $user)
                        {
                            $name = $user->user->name;
                            array_push($usernames['users'],$name);
                        }
                    }  
                    $arr['api_count'] = ProjectApi::where(['project_id' => $project->project->project_id])->count();
                    $arr['project_status'] = $project->project->project_status ?: '';
                    $arr['created_at'] = $project->project->created_at->toDayDateTimeString() ?: '';
                    $arr['users'] = implode(",",$usernames['users']) ?: "";
                    array_push($temp,$arr);
                }

                $response = [
                    'status' => 1,
                    'msg' => 'List of projects',
                    'data' => $temp,
                    'meta' => [
                        'current_page' => $projects->currentPage(),
                        'last_page' => $projects->lastPage(),
                        'path' => url('/').'/employee/list',
                    ]
                ];
            }
            else
            {
                $response = [
                    'status' => 0,
                    'msg' => 'No projects have been assigned to you yet.'
                ];
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'status' => 0,
                'msg' => $e->getMessage().' on line '.$e->getLine().' in file '.$e->getFile(),
                'data' => ''
            ];
        }
        return $response;        
    }
}
