<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Employee;
use Hash;
use App\Http\Resources\EmployeeResource;

class EmployeeController extends Controller
{
    public function index()
    {        
                        
        return view('admin.employees.employeeIndex');        
    }

    public function addEmployeePage()
    {
        return view('admin.employees.addEmployee');
    }

    public function listEmployees()
    {
        try{
            $users = User::where('user_id','!=',1)->paginate(10);
            if(count($users)>0)
            {
                $response = EmployeeResource::collection($users);
            }
            else{
                $response = [
                    'status' => 0,
                    'msg' => 'No employees found. Please add an employee.',
                    'data' => ''
                ];
            }
        }catch(\Exception $e){            
            $response = [
                'status' => 0,
                'msg' => $e->getMessage()." on line ".$e->getLine()." in file ".$e->getFile(),
                'data' => ""
            ];
        }
        return $response;
    }  
    
    public function searchEmployee($q = null)
    {
        try
        {
            if($q)
            {
                $users = User::where('name','like','%'.$q.'%')->orWhere('email','like','%'.$q.'%')->paginate(10);
                return EmployeeResource::collection($users);
            }
            else
            {
                $response = [
                    'status' => 0,
                    'msg' => 'Please enter a longer search term.'
                ];
            }   
        }
        catch(\Exception $e)
        {
            $response = [
                'status' => 0,
                'msg' => $e->getMessage()." on line ".$e->getLine()." in file ".$e->getFile(),
                'data' => ""
            ];
        }
        return $response;
    }

    public function addUser(Request $request)
    {
        try{
            if(User::where(['email' => $request->email])->count()>0){
                $response = [
                    'status' => 0,
                    'msg' => 'This email already exists. Please use a different email.',
                    'data' => ''
                ];
            }
            else
            {            
                $user = new User;
                $user->name = $request->name;
                $user->email = $request->email;
                $user->password = Hash::make($request->password);
                $user->last_login = date("Y-m-d H:i:s");
                $user->role = $request->role;            
                if($user->save()){
                    $employee = new Employee;
                    $employee->user_id = $user->user_id;
                    $employee->address = $request->address;
                    $employee->mob_no = $request->mob_no;
                    $employee->latitude = $request->latitude;
                    $employee->longitude = $request->longitude;
                    $employee->save();

                    $response = [
                        'status' => 1,
                        'msg' => 'Employee successfully added.',
                        'data' => ''
                    ];
                }else{
                    $response = [
                        'status' => 0,
                        'msg' => 'We were unable to add the employee. Please try again.',
                        'data' => ''
                    ];
                }
            }
        }catch(\Exception $e){
            $response = [
                'status' => 0,
                'msg' => $e->getMessage()." on line ".$e->getLine()." in file ".$e->getFile(),
                'data' => ""
            ];
        }
        return $response;
    }

    public function editUser($user_id = NULL)
    {
        $user = User::with('employee')->find($user_id);
        
        return view('admin.employees.editEmployee')->with('user',$user);
    }

    public function updateUser(Request $request,$id = null)
    {
        try{
            if($id)
            {
                $user = User::find($id);
                $user->name = $request->name;
                $user->role = $request->role;
                $user->save();

                $employee = Employee::where(['user_id' => $id])->first();
                $employee->address = $request->address;
                $employee->latitude = $request->latitude;
                $employee->longitude = $request->longitude;
                $employee->mob_no = $request->mob_no;
                $employee->save();

                $response = [
                    'status' => 1,
                    'msg' => 'Employee record updated successfully.'
                ];
            }
            else
            {
                $response = [
                    'status' => 0,
                    'msg' => 'Invalid Arguments given. Please try again.',
                    'data' => ''
                ];
            }
        }   
        catch(\Exception $e)
        {
            $response = [
                'status' => 0,
                'msg' => $e->getMessage()." on line ".$e->getLine()." in file ".$e->getFile(),
                'data' => ""
            ];
        }
        return $response;
    }

    public function updateStatus($user_id = null)
    {
        try{
            if($user_id)
            {
                $user = User::find($user_id);
                if(count($user)>0)
                {
                    if($user->status != 1)
                    {
                        $status = 1;
                    }
                    else 
                    {
                        $status = 2;
                    }

                    $user->status = $status;
                    $user->save();

                    $response = [
                        'status' => 1,
                        'msg' => 'Account status updated.'
                    ];
                }
                else
                {
                    $response = [
                        'status' => 0,
                        'msg' => 'No such user found. Please try again.'
                    ];
                }
            }
            else
            {
                $response = [
                    'status' => 0,
                    'msg' => 'Invalid Arguments given. Please try again.',
                    'data' => ''
                ];
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'status' => 0,
                'msg' => $e->getMessage()." on line ".$e->getLine()." in file ".$e->getFile(),
                'data' => ""
            ];
        }
        return $response;
    }

    public function deleteUser($user_id = NULL)
    {
        try
        {
            $user = User::find($user_id)->delete();
            if($user){
                $response = [
                    'status' => 1,
                    'msg' => 'User deleted successfully.',
                    'data' => ''
                ];
            }
            else{
                $response = [
                    'status' => 0,
                    'msg' => 'Record was not deleted. Please try again.',
                    'data' => ''
                ];
            }
        }
        catch(\Exception $e)
        {
            $response = [
                'status' => 0,
                'msg' => $e->getMessage()." on line ".$e->getLine()." in file ".$e->getFile(),
                'data' => ""
            ];
        }
        return $response;        
    }
}
