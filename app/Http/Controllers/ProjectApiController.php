<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Project;
use App\ProjectUser;
use App\ProjectApi;
use Auth;

class ProjectApiController extends Controller
{
    public function createApiPage()
    {
        if(Auth::user()->role === 2)
        {
        	$projects = ProjectUser::with('project')->where(['user_id' => Auth::user()->user_id])->get();
        	if(count($projects)>0)
        	{
        		return view('creator.createApiPage')->with('projects',$projects);    		
        	}
        	else
        	{
        		return redirect()->route('creator.main')->with('error','You can only create API if you are assigned a project.');
        	}
        }
        else
        {
            return redirect()->route('creator.main')->with('error','You are not authorized to access this page.');
        }
    }

    public function saveApi(Request $request)
    {
    	try{
            if(Auth::user()->role === 2)
            {
                if(empty($request->parameters))
                {
                    @json_decode($request->response);
                    if(json_last_error() === JSON_ERROR_NONE)
                    {                    
                        $input = $request->all();                        
                        ProjectApi::create($input);
                        $response = [
                            'status' => 1,
                            'msg' => 'Api added successfully.'
                        ];
                    }   
                    else
                    {
                        $response = [
                            'status' => 0,
                            'msg' => 'Invalid JSON in the response field. Please try again.',
                            'data' => ""
                        ];
                    }  
                }
                else
                {
                    @json_decode($request->parameters);
                    if(json_last_error() === JSON_ERROR_NONE)
                    {
                        @json_decode($request->response);
                        if(json_last_error() === JSON_ERROR_NONE)
                        {                    
                            $input = $request->all();                            
                            ProjectApi::create($input);
                            $response = [
                                'status' => 1,
                                'msg' => 'Api added successfully.'
                            ];
                        }   
                        else
                        {
                            $response = [
                                'status' => 0,
                                'msg' => 'Invalid JSON in the response field. Please try again.',
                                'data' => ""
                            ];
                        }  
                    }
                    else
                    {
                        $response = [
                            'status' => 0,
                            'msg' => 'Invalid JSON in the parameters field. Please try again.',
                            'data' => ""
                        ];
                    }
                }                   		
            }
            else
            {
                $response = [
                    'status' => 0,
                    'msg' => 'Access Denied',
                    'data' => ""
                ];
            }
    	}
    	catch(\Exception $e)
    	{
    		$response = [
                'status' => 0,
                'msg' => $e->getMessage()." on line ".$e->getLine()." in file ".$e->getFile(),
                'data' => ""
            ];
    	}
    	return $response;
    }

    public function listProjectApi($project_id = null)
    {
    	if($project_id)
    	{
    		$project = Project::where(['project_id' => $project_id])->with('api')->first();
    		return view('creator.viewApi')->with('project',$project);
    	}
    	else
    	{
    		return "No such page exists. Please try again.";
    	}    	
    }

    public function deleteApi($api_id = null)
    {
    	if($api_id)
    	{
	    	ProjectApi::find($api_id)->delete();
	    	return redirect()->back()->with('success','Api successfully deleted.');    		
    	}
    	else
    	{
    		return "Page does not exists.";
    	}
    }

    public function editApi($api_id = null)
    {
    	if($api_id)
    	{
    		$api = ProjectApi::find($api_id);
    		return view('creator.editApiPage')->with('api',$api);
    	}
    	else
    	{
    		return "Page does not exists.";
    	}
    }

    public function updateApi(Request $request, $api_id = null)
    {
    	try 
    	{
    		if($api_id)
    		{
                if(empty($request->parameters))
                {
                    @json_decode($request->response);
                    if(json_last_error() === JSON_ERROR_NONE)
                    {
                        ProjectApi::where(['api_id' => $api_id])->update($request->all());
                        $response = [
                            'status' => 1,
                            'msg' => 'Api Updated successfully',
                            'data' => $request->project_id
                        ];    
                    }
                    else
                    {
                        $response = [
                            'status' => 0,
                            'msg' => 'Invalid JSON in response field. Please try again.'
                        ];
                    }
                }  
                else
                {
                    @json_decode($request->parameters);
                    if(json_last_error() === JSON_ERROR_NONE)
                    {
                        @json_decode($request->response);
                        if(json_last_error() === JSON_ERROR_NONE)
                        {                           
                            ProjectApi::where(['api_id' => $api_id])->update($request->all());
                            $response = [
                                'status' => 1,
                                'msg' => 'Api Updated successfully.',
                                'data' => $request->project_id
                            ];
                        }   
                        else
                        {
                            $response = [
                                'status' => 0,
                                'msg' => 'Invalid JSON in the response field. Please try again.',
                                'data' => ""
                            ];
                        }  
                    }
                    else
                    {
                        $response = [
                            'status' => 0,
                            'msg' => 'Invalid JSON in the parameters field. Please try again.',
                            'data' => ""
                        ];
                    }
                }    			
    		}
    		else
    		{
    			$response = [
    				'status' => 0,
    				'msg' => 'Invalid arguments given. Please try again.'
    			];
    		}
    	} 
    	catch (\Exception $e) 
    	{
    		$response = [
                'status' => 0,
                'msg' => $e->getMessage()." on line ".$e->getLine()." in file ".$e->getFile(),
                'data' => ""
            ];
    	}
    	return $response;
    }
}
