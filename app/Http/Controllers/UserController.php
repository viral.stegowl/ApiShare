<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use Validator;
use App\User;

class UserController extends Controller
{
    public function loginFormPage()
    {
        return view('login.main');
    }

    public function authenticate(Request $request)
    {
        try{
            $validator = Validator::make($request->all(),[
                'email' => 'required|max:255',
                'password' => 'required'
            ]);

            if($validator->fails()){                
                $response = [
                    'status' => 0,
                    'msg' => 'Missing Arguments. Please try again.',
                    'data' => $validator->errors()
                ];
            }
            else{
                if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
                    if(Auth::user()->status == 1){
                        User::find(Auth::user()->user_id)->update(['last_login' => date("Y-m-d H:i:s")]);
                        $response = [
                            'status' => 1,
                            'msg' => 'Login successful. Redirecting...',
                            'data' => Auth::user()
                        ];    
                    }
                    else{
                        Auth::logout();
                        $response = [
                            'status' => 0,
                            'msg' => 'Your Account has been deactivated. Please ask the admin to activate it.',
                            'data' => "" 
                        ];
                    }                    
                }
                else{
                    $response = [
                        'status' => 0,
                        'msg' => 'Invalid Credentials.Please try again.',
                        'data' => ''
                    ];
                }
            }
        }catch(\Exception $e){
            $response = [
                'status' => 0,
                'msg' => 'Something has gone wrong! Please try again',
                'data' => $e->getMessage()." on line ".$e->getLine()." in file ".$e->getFile()
            ];
        }            
        return $response;        
    }

    public function logoutUser()
    {
        Auth::logout();
        return redirect()->route('login.form')->with('success','Successfully Logged Out.');
    }
}

