<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectApi extends Model
{
    public $table = "project_api";
    protected $primaryKey = "api_id";

    protected $fillable = ['project_id','user_id','endpoint','type','parameters','response','description'];

    public function project()
    {
    	return $this->belongsTo('App\Project','project_id');
    }
}
