<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProjectUser extends Model
{
    public $table = "project_user";
    protected $primaryKey = "assign_id";

    protected $fillable = ['project_id','user_id'];

    public function project()
    {
        return $this->belongsTo('App\Project','project_id');        
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
