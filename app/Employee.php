<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    public $table = "employees";
    protected $primaryKey = "emp_id";

    protected $fillable = ['user_id','address','mob_no','latitude','longitude'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id');
    }
}
