<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public $table = "projects";
    protected $primaryKey = "project_id";

    protected $fillable = ['project_name','project_description','project_status'];

    public function api()
    {
    	return $this->hasMany('App\ProjectApi','project_id');
    }
}
