<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('guest')->group(function(){
    Route::get('/', 'UserController@loginFormPage')->name('login.form')->middleware('guest');
    Route::post('/verify','UserController@authenticate')->name('login.auth');
});
Route::get('/logout','UserController@logoutUser')->name('logout');

Route::middleware('auth')->group(function(){
    Route::get('/change','DashboardController@changePassword')->name('change.password');
    Route::post('/change','DashboardController@updatePassword')->name('update.password');
    Route::middleware('onlyAdmin')->prefix('admin')->group(function(){
        //Dashboard
        Route::get('/dashboard','DashboardController@dashboardIndex')->name('admin.dashboard');

        //Employee page
        Route::prefix('employees')->group(function(){
            Route::get('/','EmployeeController@index')->name('admin.emp.index');
            Route::get('/list','EmployeeController@listEmployees')->name('admin.emp.list');
            Route::get('/search/{q?}','EmployeeController@searchEmployee');
            Route::get('/add','EmployeeController@addEmployeePage')->name('admin.emp.addPage');
            Route::post('/','EmployeeController@addUser')->name('admin.emp.add');
            Route::get('/edit/{user_id?}','EmployeeController@editUser');
            Route::post('/update/{user_id?}','EmployeeController@updateUser');
            Route::get('/changeStatus/{user_id?}','EmployeeController@updateStatus');
            Route::delete('/delete/{user_id?}','EmployeeController@deleteUser')->name('admin.emp.delete');
        });   
        
        //Project Page
        Route::prefix('projects')->group(function(){
            Route::get('/','ProjectController@indexPage')->name('admin.projects.index');
            Route::get('/list','ProjectController@listProjects')->name('admin.projects.list');
            Route::get('/search/{q?}','ProjectController@searchProjects');
            Route::get('/add','ProjectController@addProjectPage')->name('admin.projects.addPage');
            Route::post('/','ProjectController@addProject')->name('admin.projects.add');
            Route::get('/edit/{project_id?}','ProjectController@editProject');
            Route::post('/update/{project_id?}','ProjectController@updateProject');  
            Route::delete('/delete/{project_id?}','ProjectController@deleteProject')->name('admin.projects.delete');
        });

        //Assign Project Page
        Route::prefix('assign')->group(function(){
            Route::get('/','ProjectUserController@assignPage')->name('admin.assign.page');
            Route::post('/','ProjectUserController@assignUser')->name('admin.assign.user');
        });
    });

    Route::middleware('onlyCreator')->prefix('employee')->group(function(){
        //Creators assigned projects route.
        Route::get('/projects','ProjectController@listCreatorProjects')->name('creator.main');
        Route::get('/list','ProjectController@getCreatorsProjects')->name('creator.list.projects');
        Route::get('/project/{project_id?}','ProjectApiController@listProjectApi');

        //Creators new Api creation routes.
        Route::prefix('api')->group(function(){
            Route::get('/create','ProjectApiController@createApiPage')->name('creator.api.page');
            Route::post('/create','ProjectApiController@saveApi')->name('creator.api.save');
            Route::get('/delete/{api_id?}','ProjectApiController@deleteApi')->name('creator.api.delete');
            Route::get('/edit/{api_id?}','ProjectApiController@editApi')->name('creator.api.edit');
            Route::post('/update/{api_id?}','ProjectApiController@updateApi');
        });
    });
});
